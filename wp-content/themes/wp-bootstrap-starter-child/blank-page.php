<?php
/**
 * Template Name: Home Personalizado
 */

get_header();
?>

    <section id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
           <section id="slider" class="slider">
           <?php echo do_shortcode('[rev_slider alias="slider-1"][/rev_slider]');?>
           </section>
           <section id="certifi" class="certificaciones">
           <?php echo do_shortcode('[sc name="Certificaciones"]');?>
           </section>
           <section id="Aliados" class="aliados">
           <?php echo do_shortcode('[sc name="Aliados"]');?>
           </section>
           <section id="Kansy" class="kansy">
           <?php echo do_shortcode('[sc name="Kansy"]');?>
           </section>

           <section id="Itague" class="itague">
           <?php echo do_shortcode('[sc name="Itague"]');?>
           </section>

        </main><!-- #main -->
    </section><!-- #primary -->

<?php
get_footer();



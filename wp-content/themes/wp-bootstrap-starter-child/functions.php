<?php

add_action( 'wp_print_styles',  'my_style' );
function my_style(){
    wp_enqueue_style('Animate', 'https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css',false);
    }

add_action("wp_enqueue_scripts", "agregar_wow_js");

function agregar_wow_js(){

    wp_register_script('wowjs', get_stylesheet_directory_uri(). '/inc/assets/js/wow.min.js', array('jquery'), '1', true );
    wp_enqueue_script('wowjs');

}

add_action("wp_enqueue_scripts", "agregar_mi_script");

function agregar_mi_script(){

    wp_register_script('miscript', get_stylesheet_directory_uri(). '/inc/assets/js/mi-script.js', array('jquery'), '1', true );
    wp_enqueue_script('miscript');

}